
# coding: utf-8

# In[95]:

import numpy as np

def count_subchains(N,picked_links):
    s = [' ']*N
    for link in picked_links: s[link]='-'
    chain = ''.join(s)
    number_of_subchains = len(chain.strip().split())
    return number_of_subchains, chain

def draw_sample_of_M(N,show_output=False):
    remainaing_links = range(N)
    picked_links = list()
    M = 0
    for k in range(N):
        # pick a link from the remaining links at random
        q = np.random.randint(low=0,high=len(remainaing_links)) #0,1,..,high-1
        link = remainaing_links.pop(q)
        # add the link to the previously picked links
        picked_links.append(link)
        # count the subchains
        number_of_subchains, chain = count_subchains(N,picked_links)
        # keep track of the maximum number of subchains so far
        M=max(M,number_of_subchains)
        if show_output==True:
            print "|%s| current: %d max: %d"                     % (chain,number_of_subchains,M)
    return M

# draw_sample_of_M(N=8,show_output=True)
# example of the output
# |   -    | current: 1 max: 1
# |   -   -| current: 2 max: 2
# | - -   -| current: 3 max: 3
# | ---   -| current: 2 max: 3
# |----   -| current: 2 max: 3
# |-----  -| current: 2 max: 3
# |------ -| current: 2 max: 3
# |--------| current: 1 max: 3

#%% Compute sample mean and sample variance
# sample mean mu_bar = (M_1 + M_2 + ... M_n)/n
# sample variance sigma2_bar = sum_{i=1}^{n} (M_i - mu_bar)^2/n [biased]

#pip install mpmath
from mpmath import mp
mp.dps = 30

def compute_estimates(N=8,n_max=100):
    #n_max = maximum number of samples
    from math import sqrt
    s1 = 0
    s2 = 0
    s1_hp = mp.mpf(0)
    #print "%-12s,%-12s,%s" %("mean","sigma","n") 
    for n in range(1,n_max+1):
        M_n = draw_sample_of_M(N=8)
        s1 = s1 + M_n
        mu_bar = s1*1.0/n
        s1_hp = mp.fadd(s1_hp, M_n)
        mu_bar_hp = mp.fdiv(s1_hp, n)
        s2 = s2 + M_n*M_n
        if n==1: continue
        #sigma2_bar =  s2*1.0/n - mu_bar**2
        #sigma2_bar_hp = mp.fdiv(s2, n, dps=10)
        #print "%.10f,%.10f,%d" % (mu_bar,sqrt(sigma2_bar),n)
        if n%(10*1000)==0:
            print "%-20s," % mp.nstr(mu_bar_hp,15),
            print "%.10f, %d" % (mu_bar,n)
        

#compute_estimates()
#import subchains
#subchains.compute_estimates(N=8,n_max=10*1000)
#python -c "import subchains_highprec as sc;sc.compute_estimates(N=8,n_max=10*1000)"


# In[100]:

compute_estimates(N=8,n_max=10*1000)


# In[99]:

if __name__ == "__main__":
    from mpmath import mp
    mp.dps = 50 # decimal precision
    x = 1e9
    y = 1e-9
    print "decimal precision: %d" % mp.dps
    print "binary precision: %d" % mp.prec
    z = x+y
    z_hp = mp.fadd(x,y)
    print z
    print z_hp
    print "%.10f" % (z)
    print "%.10f" % (z_hp)
    mp.nprint(z_hp,35)
    s = mp.nstr(z_hp,35)
    print s

    z = 1000000000.0000000010
    print z
    print "%.25f" % (z)

    import numpy as np
    print np.exp(1)
    print mp.exp(1, dps=15)
    print mp.exp(1, dps=50)      # Extra digits won't be printed
    mp.nprint(mp.exp(1, dps=50), 50)

