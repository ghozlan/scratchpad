
# coding: utf-8

# In[1]:

import numpy as np

def count_subchains(N,picked_links):
    s = [' ']*N
    for link in picked_links: s[link]='-'
    chain = ''.join(s)
    number_of_subchains = len(chain.strip().split())
    return number_of_subchains, chain

def draw_sample_of_M(N,show_output=False):
    remainaing_links = range(N)
    picked_links = list()
    M = 0
    for k in range(N):
        # pick a link from the remaining links at random
        q = np.random.randint(low=0,high=len(remainaing_links)) #0,1,..,high-1
        link = remainaing_links.pop(q)
        # add the link to the previously picked links
        picked_links.append(link)
        # count the subchains
        number_of_subchains, chain = count_subchains(N,picked_links)
        # keep track of the maximum number of subchains so far
        M=max(M,number_of_subchains)
        if show_output==True:
            print "|%s| current: %d max: %d"                     % (chain,number_of_subchains,M)
    return M

# draw_sample_of_M(N=8,show_output=True)
# example of the output for N=8
# |   -    | current: 1 max: 1
# |   -   -| current: 2 max: 2
# | - -   -| current: 3 max: 3
# | ---   -| current: 2 max: 3
# |----   -| current: 2 max: 3
# |-----  -| current: 2 max: 3
# |------ -| current: 2 max: 3
# |--------| current: 1 max: 3

#%% Compute sample mean and sample variance
# sample mean mu_bar = (M_1 + M_2 + ... M_n)/n
# sample variance sigma2_bar = sum_{i=1}^{n} (M_i - mu_bar)^2/n [biased]
def compute_estimates(N=8,n_max=1000):
    #n_max = maximum number of samples
    print("N=%d" % N)
    from math import sqrt
    s1 = 0
    s2 = 0
    print "%-12s,%-12s,%s" %("mean","sigma","n") 
    for n in range(1,n_max+1):
        M_n = draw_sample_of_M(N=N)
        s1 = s1 + M_n
        mu_bar = s1*1.0/n
        s2 = s2 + M_n*M_n
        if n==1: continue
        sigma2_bar =  s2*1.0/n - mu_bar**2
        if n%(10*1000)==0:
            print "%.10f,%.10f,%d" % (mu_bar,sqrt(sigma2_bar),n)

# N=8, N=16, N=32
compute_estimates(N= 8,n_max=100*1000)
compute_estimates(N=16,n_max=100*1000)
compute_estimates(N=32,n_max=100*1000)

# to run from shell
#python -c "import subchains as sc;sc.compute_estimates(N=8,n_max=1000*1000)"

