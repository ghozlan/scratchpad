
# coding: utf-8

# In[1]:

get_ipython().magic(u'pylab inline')


# In[2]:

import numpy as np
import pandas as pd


# In[3]:

#%% Load train and test sets
path = "input/"
test_df = pd.read_csv(path+"test_set.csv",header=0,index_col=None)
train_df = pd.read_csv(path+"train_set.csv",header=0,index_col=None)

def nrow(x): return x.shape[0]
train_df['id'] = -np.arange(1,nrow(train_df)+1)
test_df['cost'] = 0

raw_data = pd.concat([train_df,test_df],ignore_index=True)


# In[4]:

#%% Merge files
data = raw_data.copy()
import os
raw_file_list = os.listdir(path)
import re
file_list = [f for f in raw_file_list if re.search(r"csv$",f)!=None]

continue_loop = True
while(continue_loop):
    continue_loop = False
    for f in file_list:
        df = pd.read_csv(path+f,header=0,index_col=None)
        common_variables = set(data.columns).intersection(set(df.columns))
        if(len(common_variables)==1):
            common_variable = common_variables.pop()
            if common_variable in ['length','name']: break
            #data = data.merge(df,how='left',on=common_variable)
            data = pd.merge(data,df,how='left',on=common_variable)
            shp = data.shape
            print("%dx%d after left join with %s on % s" %(shp[0],shp[1],f,common_variable))
            #print("Left join %s on % s" %(f,common_variable))
            continue_loop = True


# In[5]:

#%% Handle NA values
# DataFrame.dtypes is (pandas) Series
def isnumeric(x): return x!=np.dtype('object')

col_names = data.columns 
#col_names = [col_names[7]]
for col_name in col_names:
    isnum = isnumeric(data.dtypes[col_name])
    print("%s %s numeric" %(col_name, "is" if isnum else "is not"))
    if (isnum):
        data[col_name].fillna(-1,inplace=True) # default: inplace=False
    else:
        data[col_name].fillna("NA",inplace=True) # default: inplace=False


# In[6]:

#%% Keep only top categories for categorical variables
col_names = data.columns 
for col_name in col_names:
    isnum = isnumeric(data.dtypes[col_name])
    if (not isnum):
        print("%s is not numeric" %(col_name))
        top =  data[col_name].value_counts()[:20]
        print top
        istop = data[col_name].map(lambda x: x in top.index)
        data[col_name][~istop] = 'rare'


# In[7]:

def isnumeric(x): return x!=np.dtype('object') # ALREADY DEFINED ABOVE
column_isnumeric = data.dtypes.map(isnumeric)
numeric_col_idx = np.where(column_isnumeric)
categorical_col_idx = np.where(~column_isnumeric)
# print column_isnumeric
# print numeric_col_idx
# print categorical_col_idx


# In[8]:

#%% Change all entries to numbers (for every variable, map each category to a number)
data_allnum = data.copy()

mappings = dict()
col_names = data.columns[categorical_col_idx]
for col_name in col_names:
    categories = data[col_name].unique()
    mapping = dict(zip(categories,range(len(categories))))
    #store mapping to be able to look up 
    #what a number means for a given (categorical) column
    mappings[col_name] = mapping
    new_col = data[col_name].map(mapping)
    data_allnum[col_name] = new_col
    #data[col_name] = Categorical(new_col)

d = data_allnum.drop(['id','cost'],axis=1)
categorical_features_idx = [i for (i,c) in enumerate(d.columns) if column_isnumeric.xs(c)==False]
X_unencoded = d.as_matrix()


# In[9]:

from sklearn.preprocessing import OneHotEncoder
enc = OneHotEncoder(categorical_features=categorical_features_idx)
enc.fit(X_unencoded)

X_encoded = enc.transform(X_unencoded)
data_allnum_enc = pd.DataFrame(X_encoded.toarray())
data_allnum_enc['id'] = data_allnum['id']
# data_allnum_enc['id'] is float64 even though data_allnum['id'] is int64
# may be pandas does this automatically because 
# the rest of the columns are float
# convert float to int
# data_allnum_enc['id'] = data_allnum['id'].astype(np.int) # gives error!
data_allnum_enc['cost'] = data_allnum['cost']


# In[10]:

dataset = data_allnum_enc
test = dataset[dataset['id']>0]
train = dataset[dataset['id']<0]


# In[11]:

#----------------------------------------
# split dataset into
# training : validation : test
# 0.8      : 0.2*0.5    : 0.2*0.5
# 80%      : 10%        : 10%   
# X: features, y: target
y = train['cost'].as_matrix()
y = np.log(1+y)
X = train.drop(['id','cost'],axis=1).as_matrix()
from sklearn import cross_validation
X_train, X_rest, y_train, y_rest = cross_validation.train_test_split(X,y,train_size=0.8,random_state=0)
X_valid, X_test, y_valid, y_test = cross_validation.train_test_split(X_rest,y_rest,train_size=0.5,random_state=0)

#----------------------------------------
# define the evaluation metric (RMSLE)
from numpy import sqrt,mean,log
def rmsle(p,a): # p,a : numpy arrays
    return sqrt(mean( (log(p+1)-log(a+1))**2 ))

#----------------------------------------
# choose model and hyper-parameters

# evaluate performance on validation set
def evaluate(model): 
    # train model 
    model.fit(X_train, y_train)
    #-------------------------------------
    # evaluate performance
    predicted = model.predict(X_valid)
    predicted = np.exp(predicted)-1 
    actual = np.exp(y_valid)-1
    err = rmsle(p=predicted,a=actual)
    #print("RMSLE: %f" %(err) )
    return err

# hyper-parameter tuning
from sklearn.ensemble import RandomForestRegressor
err_list = list()
n_range = range(10,50,5)
for n in n_range:
    model = RandomForestRegressor(n_estimators=n,criterion="mse")
    err = evaluate(model)
    err_list.append(err)
    print("n_estimators=%d : RLMSE=%f" % (n,err))


# In[12]:

# Select best hyper-parameter value
min_err, n_star = sorted(zip(err_list,n_range))[0]
print(n_star)
#----------------------------------------
# train model 
model = RandomForestRegressor(n_estimators=n_star,criterion="mse")
model.fit(X_train, y_train)
#-------------------------------------
# evaluate performance on test set (generated from earlier split)
predicted = model.predict(X_test)
predicted = np.exp(predicted)-1
actual = np.exp(y_test)-1
err = rmsle(p=predicted,a=actual)
print("RMSLE: %f" %(err) )
#----------------------------------------
# visualize
import matplotlib.pylab as plt
plt.scatter(actual, predicted)
plt.xlabel('True cost')
plt.ylabel('Predicted cost')
#plt.axis('tight')
fig_list = plt.plot([0, 1000], [0, 1000], '--k')
fig = fig_list.pop()
fig.axes.set_xlim(left=0,right=50)
fig.axes.set_ylim(bottom=0,top=50)
plt.show()


# In[13]:

#----------------------------------------
# predict (for submission)
features = test.drop(['id','cost'],axis=1).as_matrix()
predicted = model.predict(features)
predicted = np.exp(predicted)-1
predicted[np.where(predicted<0)] = 1 # set negative price values to 1 
                                     # because log(negative) = -infinity


# In[14]:

# write as csv file
pd.set_option('mode.chained_assignment',None)
test['cost'] = predicted
submit = test[['id','cost']]
submit['id'] = submit['id'].astype(np.int) # convert from float to int
submit.to_csv("allcsv-enc-rf-split.csv",index=False)

