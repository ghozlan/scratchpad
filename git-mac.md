* Config and clone

```
git config --global user.name "ghozlan"
git config --global user.email "hassan.ghozlan@gmail.com"

git clone https://bitbucket.org/ghozlan/wltv-matcore
cd wltv-matcore
git branch --all --verbose
```
The repo will be at branch `master`.

* Switch to a remote branch to edit it locally then push it back to remote

```
git fetch
git checkout lti-pa #doing checkout without fetch will result in a detached head
# do changes
git status
git add .
git status
git commit -m "LTI power alloc using CVX"
git log --oneline
git push origin
```

* Create a new branch locally then push it to remote (create new branch at remote)

```
git branch ltv-pa #create new banch
git checkout ltv-pa 
#git checkout -b ltv-pa #combines creating new branch and switching to (checking out) that branch
git status
# do changes
git status
git add .
git commit -m "LTV power alloc using CVX: OPT, EB"
git log --oneline
git push origin ltv-pa # should use -u?
```