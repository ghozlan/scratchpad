cd ~
exec bash

virtualenv virtenv/scienvnew
source virtenv/scienvnew/bin/activate

pip install ipython[notebook]
pip install numpy
pip install scipy
pip install matplotlib
pip install pandas
pip install sklearn

#deactivate
