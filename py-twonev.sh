cd ~
curl https://bootstrap.pypa.io/get-pip.py > get-pip.py 
python get-pip.py --user
#find ~ -name pip #on mac: use to locate where pip is
export LOCAL_PYTHON=~/Library/Python/2.7
export PATH=$PATH:$LOCAL_PYTHON/bin

echo "export LOCAL_PYTHON=~/Library/Python/2.7" >> ~/.bashrc
echo "export PATH=\$PATH:\$LOCAL_PYTHON/bin" >> ~/.bashrc # backslash before $ to escape the $
#exec bash

pip install --user virtualenv # --user: installs to the user directory not globally
mkdir virtenv
virtualenv virtenv/scienv
source virtenv/scienv/bin/activate

pip install ipython[notebook]==2.1.0
pip install numpy
pip install scipy
pip install matplotlib
pip install pandas
pip install sklearn

deactivate
cd ~
#exec bash

virtualenv virtenv/scienvnew
source virtenv/scienvnew/bin/activate

pip install ipython[notebook]
pip install numpy
pip install scipy
pip install matplotlib
pip install pandas
pip install sklearn

deactivate
